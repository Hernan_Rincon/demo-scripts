from icmplib import ping, multiping
import logging as lg
import schedule
import time
import traceback
from multiprocessing import Process


#Script Description: pings a set of specified devices at regular intervals
#Author: Hernan Rincon, 7/10/21


#outputs ping results to the command line
#(can be replaced with an output to a database)
#params:
#host (icmplib Host object): the host object being reported
def report_ping_result(host):
    print(time.asctime(time.localtime()))
    print("IP address:",host.address)
    print(f"average roundtime trip: {host.avg_rtt} ms")
    print(f"maximum roundtime trip: {host.max_rtt} ms")
    print("packet loss:",host.packet_loss)
    print("host is reachable:",host.is_alive)
    print("\n")

#outputs multiping results to the command line
#(can be replaced with an output to a database)
#params:
#hosts (list of icmplib Host objects): the host objects being reported
def report_multiping_result(hosts):
    for host in hosts:
        report_ping_result(host)

#runs a scheduled ping task
def run():
    #the error logging output
    lg.basicConfig(filename="test.log",level=lg.WARNING)
    while True:
        try:
            #run scheduled pings
            schedule.run_pending()
            time.sleep(1)
        except Exception:
            #there are numerous exceptions associated with the icmplib ping function, here they are recorded
            exc = traceback.format_exc()
            exc_time = time.asctime(time.localtime())
            lg.warning(f" {exc_time}\n{exc}")

#schedules ping and records output
#params:
#args[0] (int or float): the time to wait between pings
#kwargs: kwargs are passed to the ping function  
def ping_handler(*args,**kwargs):
    #a pinging operation for a specific device
    do_ping = lambda: ping(**kwargs)
    #the outputted information from the ping
    out = lambda: report_ping_result(do_ping())
    #a regular schedule for performing the ping
    schedule.every(args[0]).seconds.do(out)
    #begin schedule
    run()

#schedules multiping and records output
#args[0] (int or float): the time to wait between multipings
#kwargs: kwargs are passed to the multiping function 
def multiping_handler(*args,**kwargs):
    #a pinging operation for a specific device
    do_multiping = lambda: multiping(**kwargs)
    #the outputted information from the ping
    out = lambda: report_multiping_result(do_multiping())
    #a regular schedule for performing the ping
    schedule.every(args[0]).seconds.do(out)
    #begin schedule
    run()

if __name__ == "__main__":
    #parallel pinging processes
    process_1 = Process(target=ping_handler,args=(5,),kwargs={"address":'172.217.14.78',"count":4, "interval":1, "timeout":2})
    process_2 = Process(target=ping_handler,args=(5,),kwargs={"address":'1.1.1.1',"count":4, "interval":1, "timeout":2})
    process_3 = Process(target=multiping_handler,args=(10,),kwargs={"addresses":['172.217.18.206',"104.244.42.129"],"count":4, "interval":1, "timeout":2})
    #begin processes
    process_1.start()
    process_2.start()
    process_3.start()
    #wait for end of processes (not reached with current code)
    process_1.join()
    process_2.join()
    process_3.join()


import pysnmp.hlapi as hl
import sys
import traceback

#Script description: A test of the pysnmp getCmd method for SNMP v2 communication
#Intended to be made a more extensive script once the programmer has access to SNMP agents
#Author: Hernan Rincon, 7/10/21

if __name__ == "__main__":
    #takes in an ip address, community string, and OID from the command line arguments
    ip_address,community_string,object_identifier=sys.argv[1:]
    try:
        get=hl.getCmd(hl.SnmpEngine(),
                      hl.CommunityData(community_string),
                      hl.UdpTransportTarget((ip_address,161)),
                      hl.ContextData(),
                      hl.ObjectType(hl.ObjectIdentity(object_identifier)))
        out = next(get)
        print(out)
    except Exception:
        traceback.print_exc()